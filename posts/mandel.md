---
title: mandel
description: a mandelbrot fractal image generator
project: https://gitlab.com/iodityra/daedal
date: 2020-12-05
tags:
  - c++
  - daedal
layout: layouts/post.njk
image: /blogposting/img/borked.png
---

This project was started in August of 2020 and is still in a rough state, but its going somewhere atleast.

```cpp
Complex c;
c.real_part = 1;
c.real_part = 1;

c.squared(&x,&i);
cout << x << " + " << i << "i" << endl;

int count = 0;
while (count < iterations) {
    z = z*z + c;
    count++;
}

if (z > 2) {
    return 1;
} else {
    return 0;
}
```

...not realising that there was already a library for complex numbers in c++

This is what i managed to make:

<div class='img-container'>
    <img src='/blogposting/img/undertale.png' alt='100x100'></img>
</div>

ill take a short moment to explain what this 100 by 100 pixel image is...
essentially, we iterate for each pixel on the screen, as a complex number, and each pixel will have one of two outcomes:

1. it blows up, and the number keeps getting bigger and bigger forever
2. it never increases beyond a size of 2 (this is why we have an arbitrary stop limit)

if we take the number of iterations it took to increase beyond 2 and assign a colour or shade to it, then we can give depth to the set like this:

![Greyscale Mandelbrot](/blogposting/img/grayscale.png)
Image processing was a nightmare but i ended up settling with putting all of the calculated pixels into an array and putting that into a pgm (Portable Gray Map) since getting the set to be colourful was another step for the future.

more information about the mandelbrot set can be found here:
https://en.wikipedia.org/wiki/Mandelbrot_set

Using the same escape time algorithm from before i generated that image, played around with the set a bit and made some cool stuff like:

<div class='img-container'>
    <img src='/blogposting/img/20000.png' alt='20000 iterations'></img>
</div>

Not much progress was made from this point until very recently.
I decided to rewrite the original algorithm in Julia (how fitting) and ended up rewriting the C++ surrounding it too.

While rewriting the code i implemented a settings.lua file so i didnt have to recompile every time i wanted to render a different part of the set.

Thanks to this wonderful page, https://en.wikipedia.org/wiki/Plotting_algorithms_for_the_Mandelbrot_set
_and a lot of struggling with a .ppm files_ i was able to generate the set with smooth colouring!!

<div class='img-container'>
    <img src='/blogposting/img/purple.png' alt='purple mandelbrot!'></img>
</div>

a massive improvement over the original, its much slower to generate but the new algorithm improves the look of the set considerably.

![borked](/blogposting/img/borked.png)
although theres a big problem with this algorithm, which we will look over for now.....<br>
at higher zoom levels the entire set looks completely broken.<br>
this is the new function, this entire thing needs to undergo a rewrite

but, as a bonus added i code to also render Julia sets!!!
(very similar to mandelbrot)

<div class='img-container'>
    <img src='/blogposting/img/julia.png' alt='julia set!!!'></img>
</div>

Then I implemented Julia multithreading but made negligible difference on my little i5. Will do more testing with other CPUs in future.

That is all for now :>
