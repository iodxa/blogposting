---
title: introducing daedal
description: the best thing ive ever made
project: https://gitlab.com/iodityra/daedal
date: 2021-11-23
tags:
  - daedal
  - rust
  - sdl2
layout: layouts/post.njk
image: /blogposting/img/img6.png
---

<div class='img-container'>
    <img src='/blogposting/img/img1.png' alt='100x100'></img>
</div>

its been a while...
a new name for the program, Daedal

I have discovered the best language, Rust

my entire program has been rewritten in rust, the best language ever

i now disown c++

the features ive implemented into my program are, multithreading, SDL2 graphics and dynamic window, full terminal application and animation and screenshot functions
ALL of these were made possible due to rust

the crates i used structopt for command line flags
image crate for outputting images into png format
sdl2 for displaying a window and capturing input
and egui, which has yet to be implemented but is next on the feature list

[heres a demo of the new program]

you can fully move around the environment and zoom in to the depth of an f64 and take screenshots to 4x resolution with the 's' key

[animation]

its still a little slow, but we're working on optimisations, planning to implement caching and a way to increase the maximum depth

[img of maximum depth]

# Multithreading

my god, was multithreading easy in rust. I tried in other languages (c++) but i literally just couldnt do it and moved on to something else.
but, i didnt have a giant improvement straight away. The biggest problem I had was that each thread had to update the entire image buffer each time, in order to speed this up, i made sure i was only looping through the sections that were being processed.

```rust
/// given an iterator of ImgSec, add each ImgSec to the reference imgbuf
fn receive_imgbuf<I>(receiver: I, imgbuf: &mut ImageBuffer<Rgb<u8>, Vec<u8>>)
where
    I: IntoIterator<Item = ImgSec>, {
    for img in receiver {
        // iterate through all the pixels in the buffer of the image section
        for (x, y, p) in img.buf.enumerate_pixels() {
            // add the processed pixels to imgbuf with a position offset
            imgbuf.put_pixel(x + img.x, y + img.y, *p);
        }
    }
}
```

I made this comfy little function, that takes an iterator as its first argument and modifies the reference to the image buffer.

I made a custom type for this, called ImgSec, which is a section of the main image buffer with an offset so that we can process it without having to iterate through the main image buffer for **_every single image section_**.

The purpose of the iterator as an argument is such that we dont have to always process the image sections in the same way, sometimes we might want to only process sections that are readily available but other times, like when taking a screenshot, we want to wait and process all possible buffers.

# structopt

[img of structopt]

this is the best thing i have ever found for making command line applications, thank you so much rust community.

# colourscheme

i finally implemented proper colours!!!

[cool coloured imag]

this was a simple addition that dramatically improved the quality of my images:

[more]

and i made a way you can make a colourscheme! (although as of right now it involves editing the source code)
